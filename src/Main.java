import java.io.*;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.ArrayList;

public class Main
{
	public static void main(String[] args)
	{
		//args = new String[] {"-encrypt", "Shea.jpg", "josefvencasladek", "cypher.data"};
		//args = new String[] {"-decrypt", "cypher.data", "josefvencasladek", "decrypted-Shea.jpg"};
		
		//args = new String[] {"-encrypt", "message.txt", "josefvencasladek", "cypher.data"};
		//args = new String[] {"-decrypt", "cypher.data", "josefvencasladek", "decrypted-message.txt"};
		
		if(args.length != 4)
		{
			System.out.println("Usage:\n-encrypt [input_file] [key] [encrypted_file]\n" +
									   "-decrypt [encrypted_file] [key] [output_file]");
			return;
		}
		
		boolean encrypt;
		if(args[0].equals("-encrypt"))
		{
			encrypt = true;
		}
		else if(args[0].equals("-decrypt"))
		{
			encrypt = false;
		}
		else
		{
			System.out.println("First parameter must be -encrypt or -decrypt");
			return;
		}
		
		if(!Files.exists(Path.of(args[1])))
		{
			System.out.println("Input file does not exist");
			return;
		}
		
		if(args[2].length() != 16)
		{
			System.out.println("Key must be 16 characters long");
			return;
		}
		
		Path output;
		try
		{
			output = Path.of(args[3]);
		}
		catch(InvalidPathException e)
		{
			System.out.println("Invalid output path");
			return;
		}
		
		int index = 0;
		int[][] key = new int[4][4];
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				key[i][j] = args[2].charAt(index++) & 0xff;
			}
		}
		
		File inputFile = new File(args[1]);
		
		byte[] allBytes;
		try(BufferedInputStream stream = new BufferedInputStream(new FileInputStream(inputFile)))
		{
			allBytes = stream.readAllBytes();
		}
		catch(IOException e)
		{
			System.out.println("Error while reading " + (encrypt ? "input" : "encrypted") + " file");
			return;
		}
		
		ArrayList<Byte> bytes = new ArrayList<>();
		for(byte b : allBytes)
		{
			bytes.add(b);
		}
		
		if(allBytes.length % 16 != 0)
		{
			int zeroes = 16 - allBytes.length % 16;
			for(int i = 0; i < zeroes; i++)
			{
				bytes.add((byte)0);
			}
		}
		
		int numberOfBlocks = bytes.size() / 16;
		
		int[][][] input = new int[numberOfBlocks][4][4];
		
		index = 0;
		for(int[][] block : input)
		{
			for(int[] row : block)
			{
				for(int i = 0; i < row.length; i++)
				{
					row[i] = Byte.toUnsignedInt(bytes.get(index++));
				}
			}
		}
		
		if(encrypt)
		{
			encrypt(input, key, output);
		}
		else
		{
			decrypt(input, key, output);
		}
	}
	
	private static void encrypt(int[][][] input, int[][] key, Path outputFile)
	{
		int[][][] keys = AES.createKeys(key);
		
		for(int[][] matrix : input)
		{
			AES.encrypt(matrix, keys);
		}
		
		cipherToString(input);
		
		saveFile(input, outputFile);
		
		System.out.println("Done");
	}
	
	private static void decrypt(int[][][] input, int[][] key, Path outputFile)
	{
		int[][][] keys = AES.createKeys(key);
		
		for(int[][] matrix : input)
		{
			AES.decrypt(matrix, keys);
		}
		
		saveFile(input, outputFile);
		
		System.out.println("Done");
	}
	
	public static void saveFile(int[][][] output, Path path)
	{
		byte[] bytes = new byte[output.length * output[0].length * output[0][0].length];
		
		int index = 0;
		for(int[][] block : output)
		{
			for(int[] row : block)
			{
				for(int value : row)
				{
					bytes[index++] = (byte)value;
				}
			}
		}
		
		try
		{
			Files.write(path, bytes);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void cipherToString(int[][][] cipher)
	{
		System.out.println("Encrypted data:");
		StringBuilder builder = new StringBuilder();
		for(int[][] matrix : cipher)
		{
			for(int[] row : matrix)
			{
				for(int hexValue : row)
				{
					builder.append(String.format("%02x ", hexValue));
				}
			}
			System.out.println(builder);
			builder.setLength(0);
		}
	}
}
